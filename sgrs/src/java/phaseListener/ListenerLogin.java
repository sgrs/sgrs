package phaseListener;

import bean.UserBean;
import javax.faces.application.Application;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class ListenerLogin implements PhaseListener{

    @Override
    public void beforePhase(PhaseEvent event) {
        
    }

    @Override
    public void afterPhase(PhaseEvent event) {

        FacesContext ctx = FacesContext.getCurrentInstance();

        if((!ctx.getViewRoot().getViewId().equals("/login.xhtml")) && (!ctx.getViewRoot().getViewId().equals("/forgPasswd.xhtml")) && (!ctx.getViewRoot().getViewId().equals("/newUser.xhtml"))){
            Application app = ctx.getApplication();
            UserBean u = (UserBean) app.evaluateExpressionGet(ctx, "#{userBean}", UserBean.class);

            if(!u.isLogged()){

                NavigationHandler handler = app.getNavigationHandler();
                handler.handleNavigation(ctx, "", "login?faces-redirect=true");
                ctx.renderResponse();
            }
        }
    }

    @Override
    public PhaseId getPhaseId() {
         return PhaseId.RESTORE_VIEW;
    }
    
}

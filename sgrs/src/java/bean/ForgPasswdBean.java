package bean;

import dao.UserDao;
import java.io.Serializable;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@ViewScoped
@Named("forgPasswdBean")
public class ForgPasswdBean implements Serializable {
    private String email = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void enviarEmail(){
        if(new UserDao().getUserByEmail(email)!=null){
            if(enviarGmail()){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Email enviado com sucesso!"));
            }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao enviar Email."));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Email não cadastrado."));
        }
    }
    
    public boolean enviarGmail(){
        String senha = Gerar();

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session s = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication(){

                        return new PasswordAuthentication("sistemacadastror@gmail.com", "@lun0D0S3rvid0r");
                    }
                });
        try {
            MimeMessage message = new MimeMessage(s);
            message.setFrom(new InternetAddress("sistemacadastror@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(this.email));

            message.setSubject("Nova Senha");
            message.setContent(senha, "text/html; charset=utf-8");

            Transport.send(message);
            
            new UserDao().changePasswd(email, senha);

            return true;
        } catch (MessagingException e){
            e.printStackTrace();
        }
        return false;
    }
    
    public static String Gerar() {
        String[] carct ={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        String senha="";

        for (int x=0; x<10; x++){
            int j = (int) (Math.random()*carct.length);
            senha += carct[j];
        }
        return senha;
    }
    
}
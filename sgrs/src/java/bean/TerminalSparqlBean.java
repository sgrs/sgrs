package bean;

//import GraphViz.GraphViz;
import dao.NsDao;
import dao.TerminalSparqlDao;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.NsModel;
import model.tripla.TriplaModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ViewScoped
@Named("terminalSparqlBean")
public class TerminalSparqlBean implements Serializable {

    @Inject
    private UserBean userBean;

    private NsDao nsDao = new NsDao();
    private String dir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
    private String nameFile;
    private String ntriple;
    private String json;
    private String turtle;
    private String rdf;
    private String querySPARQL = "select * where {?s ?p ?o}";
    private String repository;
    private boolean reasoning;
    private boolean URI;
    private int limitResults;
    private Model model = ModelFactory.createDefaultModel();
    private List<TriplaModel> triplas = new ArrayList();
    private List<String> strings = new ArrayList();
    private StreamedContent file;
    private String typeDownload;

    public void fileDownloadView() {
        FileWriter arquivo;
        if (typeDownload.equals("RDF")) {
            try {
                arquivo = new FileWriter(dir + "/Arquivos/" + userBean.getUser().getEmail() + ".rdf");
                PrintWriter gravarArq = new PrintWriter(arquivo);
                gravarArq.printf(rdf);
                gravarArq.close();
            } catch (IOException ex) {
                Logger.getLogger(TerminalSparqlBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/Arquivos/" + userBean.getUser().getEmail() + ".rdf");
            System.out.print("####" + stream.toString());
            file = new DefaultStreamedContent(stream, "text/plain", userBean.getUser().getEmail() + ".rdf");
        } else if (typeDownload.equals("Turtle")) {
            try {
                arquivo = new FileWriter(dir + "/Arquivos/" + userBean.getUser().getEmail() + ".ttl");
                PrintWriter gravarArq = new PrintWriter(arquivo);
                gravarArq.printf(turtle);
                gravarArq.close();
            } catch (IOException ex) {
                Logger.getLogger(TerminalSparqlBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/Arquivos/" + userBean.getUser().getEmail() + ".ttl");
            System.out.print("####" + stream.toString());
            file = new DefaultStreamedContent(stream, "text/plain", userBean.getUser().getEmail() + ".ttl");
        } else if (typeDownload.equals("Json-ld")) {
            try {
                arquivo = new FileWriter(dir + "/Arquivos/" + userBean.getUser().getEmail() + ".jsonld");
                PrintWriter gravarArq = new PrintWriter(arquivo);
                gravarArq.printf(json);
                gravarArq.close();
            } catch (IOException ex) {
                Logger.getLogger(TerminalSparqlBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/Arquivos/" + userBean.getUser().getEmail() + ".jsonld");
            System.out.print("####" + stream.toString());
            file = new DefaultStreamedContent(stream, "text/plain", userBean.getUser().getEmail() + ".jsonld");

        } else if (typeDownload.equals("N-Triples")) {
            try {
                arquivo = new FileWriter(dir + "/Arquivos/" + userBean.getUser().getEmail() + ".nt");
                PrintWriter gravarArq = new PrintWriter(arquivo);
                gravarArq.printf(ntriple);
                gravarArq.close();
            } catch (IOException ex) {
                Logger.getLogger(TerminalSparqlBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/Arquivos/" + userBean.getUser().getEmail() + ".nt");
            System.out.print("####" + stream.toString());
            file = new DefaultStreamedContent(stream, "text/plain", userBean.getUser().getEmail() + ".nt");

        }
    }

    public StreamedContent getFile() {
        fileDownloadView();
        return file;
    }

    public String convertUriToPrefix(String uri) {
        for (NsModel ns : nsDao.getAllNS()) {
            if (uri.startsWith(ns.getUri())) {
                uri = uri.replace(ns.getUri(), ns.getPrefix() + ":");
                break;
            }
        }        
        return uri;
    }
    
    public void generateGraph() {
//        String type = "png";
//        nameFile = userBean.getUser().getEmail();
//        nameFile = "/Grafos/" + nameFile + "." + type;
//        GraphViz gv = new GraphViz();
//        gv.addln(gv.start_graph());
//        model.write(System.out);
//        for (StmtIterator it = model.listStatements(); it.hasNext();) {
//            Statement stmt = it.next();
//            gv.addln("<" + stmt.getSubject().getURI() + "> -> <" + stmt.getObject().toString() + "> [ label = \"" + stmt.getPredicate().getURI() + "\" ];");
//        }
//        gv.addln(gv.end_graph());
//        gv.increaseDpi();
//        System.out.println(gv.getDotSource());
//        File out = new File(dir + nameFile);    // Windows
//        byte[] x = gv.getGraph(gv.getDotSource(), type, "dot");
//        gv.writeGraphToFile(x, out);
    }

    public void generateTripla() {
        TriplaModel tripla;
        triplas.clear();
        for (StmtIterator it = model.listStatements(); it.hasNext();) {
            Statement stmt = it.next();
            if(URI){
                tripla = new TriplaModel(stmt.getSubject().getURI(), stmt.getPredicate().getURI(), stmt.getObject().toString());
            }else{
                tripla = new TriplaModel(convertUriToPrefix(stmt.getSubject().getURI()), convertUriToPrefix(stmt.getPredicate().getURI()), convertUriToPrefix(stmt.getObject().toString()));
            }
            triplas.add(tripla);
        }
    }

    public String generateNTriples() {
        StringWriter out = new StringWriter();
        model.write(out, "N-Triples");
        ntriple = out.toString();
        return ntriple;
    }

    public String generateRDF() {
        StringWriter out = new StringWriter();
        model.write(out, "RDF/XML");
        rdf = out.toString();
        return rdf;
    }

    public String generateTurtle() {
        StringWriter out = new StringWriter();
        model.write(out, "Turtle");
        turtle = out.toString();
        return turtle;
    }

    public String generateJSON() {
        StringWriter out = new StringWriter();
        model.write(out, "JSON-LD");
        json = out.toString();
        return json;
    }

    public void execute() {
        model = new TerminalSparqlDao().search(querySPARQL, limitResults, reasoning, repository);
        generateRDF();
        generateTripla();
        generateTurtle();
        generateJSON();
        generateNTriples();
        generateGraph();
    }

    public String getQuerySPARQL() {
        return querySPARQL;
    }

    public void setQuerySPARQL(String querySPARQL) {
        this.querySPARQL = querySPARQL;
    }

    public int getLimitResults() {
        return limitResults;
    }

    public void setLimitResults(int limitResults) {
        this.limitResults = limitResults;
    }

    public boolean isReasoning() {
        return reasoning;
    }

    public void setReasoning(boolean reasoning) {
        this.reasoning = reasoning;
    }

    public boolean isURI() {
        return URI;
    }

    public void setURI(boolean URI) {
        this.URI = URI;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public List<TriplaModel> getTriplas() {
        return triplas;
    }

    public void setTriplas(List<TriplaModel> triplas) {
        this.triplas = triplas;
    }

    public String getRdf() {
        return rdf;
    }

    public void setRdf(String rdf) {
        this.rdf = rdf;
    }

    public String getTurtle() {
        return turtle;
    }

    public void setTurtle(String turtle) {
        this.turtle = turtle;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getNtriple() {
        return ntriple;
    }

    public void setNtriple(String ntriple) {
        this.ntriple = ntriple;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getTypeDownload() {
        return typeDownload;
    }

    public void setTypeDownload(String typeDownload) {
        this.typeDownload = typeDownload;
    }

}

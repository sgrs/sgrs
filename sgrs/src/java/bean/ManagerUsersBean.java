package bean;

import dao.UserDao;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import model.UserModel;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.primefaces.event.CellEditEvent;

@ViewScoped
@Named("managerUsersBean")
public class ManagerUsersBean implements Serializable {

    private List<UserModel> users = new UserDao().getAll();
    private UserModel user;

    private String name = "";
    private String email = "";
    private String phone = "";
    private String passwd = "";
    private boolean adm = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public boolean getAdm() {
        return adm;
    }

    public void setAdm(boolean adm) {
        this.adm = adm;
    }

    public void limpar() {
        name = "";
        email = "";
        phone = "";
        passwd = "";
        adm = false;
    }

    public void resetPasswd(UserModel user) {
        String pass = gerarSenha();
        if (enviarGmail(user.getEmail(), pass, 2)) {
            if (new UserDao().changePasswd(user.getEmail(), pass)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Nova senha gerada com sucesso!"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao salvar nova senha para a conta do usuário."));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao enviar e-mail com nova senha !"));
        }
    }

    public void salvar() {
        if ((!name.equals(""))
                && (!email.equals(""))) {
            String pass = gerarSenha();
            if (enviarGmail(name, pass, 1)) {
                UserModel newUser = new UserModel();
                newUser.setName(name);
                newUser.setAdm(true);
                newUser.setActivated(false);
                newUser.setEmail(email);
                newUser.setPasswd(pass);
                newUser.setPhone(phone);

                if (new UserDao().insert(newUser)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Usuário cadastrado com sucesso!"));
                    limpar();
                    users = new UserDao().getAll();
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao efetuar efetuar cadastro."));
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Digite um e-mail válido !"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Campos 'nome' e 'Email' são obrigatórios !"));
        }
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public void setRPasswd(UserModel user) {

    }

    public boolean enviarGmail(String email, String senha, int tipo) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session s = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("sistemacadastror@gmail.com", "@lun0D0S3rvid0r");
            }
        });
        try {
            MimeMessage message = new MimeMessage(s);
            message.setFrom(new InternetAddress("sistemacadastror@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            if (tipo == 1) {
                message.setSubject("Criação de conta");
                message.setContent("Prezado, " + name + ", <br/>Sua conta no sistema Gerenciador de Recursos Semânticos foi criada com sucesso!<br/> Sua senha de acesso é: " + senha + "<br/>", "text/html; charset=utf-8");

                Transport.send(message);

                return true;
            }
            if (tipo == 2) {
                message.setSubject("Nova Senha");
                message.setContent("Prezado, " + name + ", <br/>Sua senha sistema Gerenciador de Recursos Semânticos foi Modificada!<br/> Sua nova senha de acesso é: " + senha + "<br/>", "text/html; charset=utf-8");

                Transport.send(message);

                return true;
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            if (new UserDao().update(users.get(event.getRowIndex()))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Dados alterados com sucesso!"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao alterar dados."));
            }
        }
    }

    public String gerarSenha() {
        String[] carct = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String senha = "";

        for (int x = 0; x < 10; x++) {
            int j = (int) (Math.random() * carct.length);
            senha += carct[j];
        }
        return senha;
    }

    public void excluir(UserModel user) {
        if (new UserDao().delete(user.getUri(), null, null)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Usuário deletado com sucesso !"));
            users = new UserDao().getAll();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Não foi possivel deletar usuário !"));
        }
    }

}

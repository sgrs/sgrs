package bean;

import dao.NsDao;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import model.NsModel;

@ViewScoped 
@Named("nsBean")
public class NsBean implements Serializable{
    private NsDao nsDao = new NsDao();
    private NsModel ns;
    
    private String newPrefixo;
    private String newUri;
    
    public void salvar(){
        if(validatePrefixAndUri()){            
            if(nsDao.insert(new NsModel(newPrefixo, newUri))){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Prefixo cadastrado com sucesso."));
                this.newPrefixo="";
                this.newUri="";
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao cadastrar prefixo."));
            }
        }
    }

    public void deleteNs(){
        if(ns==null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Selecione o prefixo a ser excluído."));
        }else{
            if(nsDao.delete(ns)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Prefixo excluído com sucesso."));
                this.ns = null;
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao excluir prefixo."));
            }
        }
    }
    
    public List<NsModel> getNss() {
        return nsDao.getAllNS();
    }

    public NsModel getNs() {
        return ns;
    }

    public void setNs(NsModel ns) {
        this.ns = ns;
    }

    public String getNewPrefixo() {
        return newPrefixo;
    }

    public void setNewPrefixo(String newPrefixo) {
        this.newPrefixo = newPrefixo;
    }

    public String getNewUri() {
        return newUri;
    }

    public void setNewUri(String newUri) {
        this.newUri = newUri;
    }

    private boolean validatePrefixAndUri(){
        if(newPrefixo==null || newPrefixo.equals("")){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Favor informar o prefixo a ser cadastrado."));
            return false;    
        }
        if(newUri==null || !newUri.startsWith("http://")){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Favor informar a uri a ser cadastrada."));
            return false;
        }
        for(NsModel ns : nsDao.getAllNS()){
            if(ns.getPrefix().equals(newPrefixo)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Prefixo já cadastrado."));
                return false;
            }
            if(ns.getUri().equals(newUri)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Uri já cadastrado."));
                return false;
            }
        }
        return true;
    }
    
    
    
}

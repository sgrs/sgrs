package bean;

import dao.ImportOntDao;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import org.primefaces.model.UploadedFile;

@ViewScoped
@Named("importOntBean")
public class ImportOntBean implements Serializable {

    private UploadedFile arquivo;
    private String tipo = "";
    private String URI = "";

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public UploadedFile getArquivo() {
        return arquivo;
    }

    public void setArquivo(UploadedFile arquivo) {
        this.arquivo = arquivo;
    }

    public void importLocal() {
        if (arquivo != null) {
            if ((arquivo.getFileName().endsWith(".rdf")) || (arquivo.getFileName().endsWith(".txt")) || (arquivo.getFileName().endsWith(".ttl"))
                    || (arquivo.getFileName().endsWith(".n3")) || (arquivo.getFileName().endsWith(".owl")) || (arquivo.getFileName().endsWith(".nt"))
                    || (arquivo.getFileName().endsWith(".jsonld"))) {

                try {

                    Model jena = ModelFactory.createDefaultModel();
                    if (tipo.equals("Auto")) {
                        jena.read(arquivo.getInputstream(), "");
                        if (new ImportOntDao().insert(jena)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ontologia importada com sucesso!"));
                        }
                    } else {
                        jena.read(arquivo.getInputstream(), tipo);
                        if (new ImportOntDao().insert(jena)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ontologia importada com sucesso!"));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ImportOntBean.class.getName()).log(Level.SEVERE, null, ex);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao importar ontologia!"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao importar ontologia!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao importar ontologia!"));
        }

    }

    public void importURI() {
        Model jena = ModelFactory.createDefaultModel();
        if (URI.startsWith("http://")) {
            try {
                jena.read(URI);
                if (jena.size() == 0) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Digite uma URI válida !"));

                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ontologia importada com sucesso !"));

                if (new ImportOntDao().insert(jena)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Ontologia importada com sucesso !"));

                }

            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Digite um URI válido!"));
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Digite um URI válido!"));

        }
    }
}

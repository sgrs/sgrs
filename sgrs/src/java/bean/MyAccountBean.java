package bean;

import dao.UserDao;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.UserModel;
    
@ViewScoped
@Named("myAccountBean")
public class MyAccountBean implements Serializable {
    
    @Inject
    private UserBean userBean;
    
    private String uri;
    private String name;
    private boolean alterPasswd = false;
    private String email;
    private String phone;
    private String passwd;
    private String newPasswd;
    private String newPasswdC;
    private String adm;  
    private String activated;
    
    @PostConstruct
    protected void inicializa(){
        uri = userBean.getUser().getUri();
        name = userBean.getUser().getName();
        alterPasswd = false;
        email = userBean.getUser().getEmail();
        phone = userBean.getUser().getPhone();
        passwd = "";
        if(userBean.getUser().isAdm()){
             adm = "1"; 
        }else{
             adm = "0"; 
        } 
        if(userBean.getUser().isActivated()){
             activated = "1"; 
        }else{
             activated = "0"; 
        } 
    }
    
    public String getAdm() {
        return adm;
    }

    public void setAdm(String adm) {
        this.adm = adm;
    }
    
    public boolean isAlterPasswd() {
        return alterPasswd;
    }

    public void setAlterPasswd(boolean alterPasswd) {
        this.alterPasswd = alterPasswd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getNewPasswd() {
        return newPasswd;
    }

    public void setNewPasswd(String newPasswd) {
        this.newPasswd = newPasswd;
    }

    public String getNewPasswdC() {
        return newPasswdC;
    }

    public void setNewPasswdC(String newPasswdC) {
        this.newPasswdC = newPasswdC;
    } 
    
    public void salvar (){
        UserDao uDao = new UserDao();
        if(uDao.logar(email, passwd)!=null){
            UserModel user = new UserModel();
            user.setUri(uri);
            user.setName(name);
            user.setEmail(email);
            user.setPhone(phone);
            user.setPasswd(passwd);
            if (alterPasswd == true){
                if(!newPasswd.equals(newPasswdC)){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Campos Nova Senha e Confirmar Nova Senha devem ser iguais."));
                }else{
                    user.setPasswd(newPasswd);
                }            
            }
            if (adm.equals("0")){
                user.setAdm(false);
            } else{
                user.setAdm(true);
            }
            if (activated.equals("0")){
                user.setActivated(false);
            } else{
                user.setActivated(true);
            }
            if(uDao.update(user)){
                userBean.setEmail(user.getEmail());
                userBean.setSenha(user.getPasswd());
                userBean.logar();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Dados atualizados com sucesso!"));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha na atualização dos dados."));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Senha atual inválida."));
        }
    }
}

package bean;

import dao.UserDao;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.UserModel;

@ViewScoped
@Named("newUserBean")
public class NewUserBean implements Serializable{
    private String name = "";
    private String email = "";
    private String phone = "";
    private String passwd = "";
    private String adm = "0";
    
    public void salvar(){
        UserModel user = new UserModel();
        user.setName(name);
        if(adm.equals("0")){
            user.setAdm(false);
        }else{
            user.setAdm(true);
        }        
        user.setActivated(false);
        user.setEmail(email);
        user.setPasswd(passwd);
        user.setPhone(phone);
        
        UserDao user1 = new UserDao();
        if(user1.insert(user)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Usuário cadastrado com sucesso!"));  
            limpar();
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao efetuar efetuar cadastro."));
        }
    }
    
    
    public void limpar(){
        name = "";
        email = "";
        phone = "";
        passwd = "";
        adm = "0";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getAdm() {
        return adm;
    }

    public void setAdm(String adm) {
        this.adm = adm;
    }


    
    
}

package bean;

import dao.FormDao;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.FormModel;

@ViewScoped
@Named("formBean")
public class FormBean implements Serializable{
    private FormModel form; 
    private List<FormModel> forms;
    
    @PostConstruct  
    protected void initialize() {  
        forms = new FormDao().getAll();   
    }

    public FormModel getForm() {
        return form;
    }

    public void setForm(FormModel form) {
        this.form = form;
    }

    public List<FormModel> getForms() {
        return forms;
    }

    public void setForms(List<FormModel> forms) {
        this.forms = forms;
    }
    
    public void deleteForm(){
        if(form!=null){
            if(new FormDao().delete(form.getUri(), null, null)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Formulário excluído com sucesso."));
                initialize();
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Falha ao excluir formulário."));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Selecione um formulário"));
        }
    }
    
    public void loadForm(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        if(ctx.getViewRoot().getViewId().equals("/newResource.xhtml")){
            Application app = ctx.getApplication();
            GraphBean g = (GraphBean) app.evaluateExpressionGet(ctx, "#{graphBean}", GraphBean.class);
            g.loadForm(form);
        }else if(ctx.getViewRoot().getViewId().equals("/resources.xhtml")){
            Application app = ctx.getApplication();
            ResourcesBean r = (ResourcesBean) app.evaluateExpressionGet(ctx, "#{resourcesBean}", ResourcesBean.class);
            r.loadForm(form);
        }
    }
    
}

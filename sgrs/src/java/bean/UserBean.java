package bean;

import dao.UserDao;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import model.UserModel;

@SessionScoped 
@Named("userBean")
public class UserBean implements Serializable{
    private UserModel user = null;
    private String email = "";
    private String senha = "";
    
    public String logar(){  
        this.user = new UserDao().logar(email, senha);
        //this.user = new UserModel();
        if(this.user!=null){
            return "index.xhtml";
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao efetuar login. Favor verificar seu email e senha."));
            return "";
        }
    }
    
    public void deslogar(){
        this.user = null;
    }
    
    public boolean isLogged(){
        if(this.user==null){
            return false;
        }else{
            return true;
        }
    }

    public UserModel getUser() {
        return user;
    }
    
    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


    
}

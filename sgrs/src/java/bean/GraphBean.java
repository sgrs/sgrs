package bean;

import dao.FormDao;
import dao.GraphDao;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import model.FormModel;
import model.GraphModel;
import model.tripla.ObjectModel;
import model.tripla.PredicateModel;
import model.tripla.SubjectModel;
import model.tripla.TriplaModel;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.model.dynaform.DynaFormRow;

@ViewScoped
@Named("graphBean")
public class GraphBean implements Serializable {
    private GraphDao gDao;
    
    private boolean saveForm;
    private String saveFormName;
    
    private DynaFormModel model;
    private GraphModel graph;

    @PostConstruct
    protected void initialize() {
        gDao = new GraphDao();
        
        saveForm = false;
        saveFormName = "";
        
        graph = new GraphModel();
        // 1. tripla and row  
        TriplaModel tripla = new TriplaModel("", "rdf:type", "");
        graph.addTripla(tripla);
        updateDynaFormModel();
    }

    public void addTripla(TriplaModel triplaAnterior, int nivel) { 
        if (nivel == 1) {
            TriplaModel tripla = new TriplaModel(triplaAnterior.getSubject());
            int indexAtual = lastIndexWithSamePredicate(triplaAnterior);
            graph.addTripla(indexAtual, tripla);
        } else if (nivel == 2) {
            TriplaModel tripla = new TriplaModel(triplaAnterior.getSubject(), triplaAnterior.getPredicate());
            int indexAtual = lastIndexWithSamePredicate(triplaAnterior);
            graph.addTripla(indexAtual, tripla);
        }
        updateDynaFormModel();
    }

    public void removeTripla(TriplaModel tripla) {
        graph.removeTripla(tripla);
        updateDynaFormModel();
    }

    public String getRange(TriplaModel tripla) {
        String range = gDao.getRangeType(tripla.getPredicate().getPredicate());
        if (range.indexOf('#') == -1) {
            range = range.substring(range.lastIndexOf('/') + 1);
        } else {
            range = range.substring(range.indexOf('#') + 1);
        }
        return range;
    }
    
    public DynaFormModel getModel() {
        return this.model;
    }

    public void updateDynaFormModel() {
        int cont = 0;
        model = new DynaFormModel();
        TriplaModel anterior = new TriplaModel();
        for (TriplaModel tripla : graph.getTriplas()){
            //String range = getRange(tripla);
            if(cont==0){
                cont++;
                DynaFormRow row = model.createRegularRow();
                row.addControl(tripla, "dominioForm");
                row.addControl(tripla, "sujeitoForm", 2, 1); 
                row.addControl(tripla, "idForm"); 
                //2ª linha para a 1ª tripla ainda
                row = model.createRegularRow();  
                row.addControl(tripla, "sujeitoFormInvisible");    
                row.addControl(tripla, "predicadoFormRead");  
                row.addControl(tripla, "objetoFormRdfType");  
                row.addControl(tripla, "op0");
            }else{
                if(!anterior.getPredicate().equals(tripla.getPredicate())){
                    DynaFormRow row = model.createRegularRow();
                    row.addControl(tripla, "sujeitoFormInvisible");    
                    row.addControl(tripla, "predicadoForm");  
                    row.addControl(tripla, "objetoForm");  
                    row.addControl(tripla, "op1");
                }else{
                    DynaFormRow row = model.createRegularRow();
                    row.addControl(tripla, "sujeitoFormInvisible");    
                    row.addControl(tripla, "predicadoFormInvisible");  
                    row.addControl(tripla, "objetoForm");  
                    row.addControl(tripla, "op2");
                }
            }
            anterior=tripla;
        }
    }

  public void salvar(){
        if(validateGraph()){
            boolean status = gDao.insert(this.graph);
            if(status){
                    if(isSaveForm()){
                        new FormDao().insert(new FormModel(this.saveFormName, this.graph.getTriplas()));
                    }
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Cadastro realizado com sucesso!"));
                    this.initialize();
            }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Falha ao cadastrar recurso."));
            }
        }
    }
  
    public boolean validateGraph(){
        for(TriplaModel tripla : graph.getTriplas()){
            if( (tripla.getSubject().getSubject().equals("")) 
                || (tripla.getPredicate().getPredicate().equals(""))
                || (tripla.getObject().getObject().equals("")) ){
                
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Todos os campos devem ser preenchidos."));
                return false;
            }
        }
        if( (this.saveForm) && (this.saveFormName.equals("")) ){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Campo 'Nome do Formulário' deve ser preenchido."));
            return false;            
        }
        return true;
    }

    public void limpar() {
        this.initialize();
    }

    private int lastIndexWithSamePredicate(TriplaModel tripla) {
        int indexAtual = graph.getTriplas().indexOf(tripla);
        for (; indexAtual < graph.getTriplas().size(); indexAtual++) {
            if (!graph.getTriplas().get(indexAtual).getPredicate().equals(tripla.getPredicate())) {
                break;
            }
        }
        return indexAtual;
    }

    public List<String> completeTextPredicate(String query) {
        return gDao.getPredicatesWithInitial(query);
    }

    public List<String> completeTextObject(String query) {
        FacesContext context = FacesContext.getCurrentInstance();
        TriplaModel tripla = (TriplaModel) UIComponent.getCurrentComponent(context).getAttributes().get("tripla");
        return gDao.getObjectsWithInitial(tripla.getPredicate().getPredicate(), query);
    }

    public void generateForm(TriplaModel tripla) {
        if (tripla.getPredicate().getPredicate().equals("rdf:type")) {
            List<String> list = gDao.getPredicatesToClass(tripla.getObject().getObject());
            graph.getTriplas().retainAll(graph.getTriplas().subList(0, 1));
            
            for (String p : list) {
                TriplaModel t = new TriplaModel(tripla.getSubject(), new PredicateModel(p));
                //int indexAtual = lastIndexWithSamePredicate(t);
                graph.addTripla(t);
            }
        }
        updateDynaFormModel();
    }
    
    public void loadForm(FormModel form) {
        if(form!=null){
            saveForm = false;
            saveFormName = "";
            
            graph = new GraphModel();  
            SubjectModel subject = new SubjectModel("");
            for(String predicate : form.getPredicates()){
                TriplaModel tripla;
                if(predicate.equals("rdf:type")){
                    tripla = new TriplaModel(subject, new PredicateModel(predicate), new ObjectModel(form.getTypeValue()));
                }else{
                    tripla = new TriplaModel(subject, new PredicateModel(predicate));
                }
                graph.addTripla(tripla);
            }
            updateDynaFormModel(); 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Formulário carregado com sucesso!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Selecione um formulário"));
        }
    }
 
    public boolean isSaveForm() {
        return saveForm;
    }

    public void setSaveForm(boolean saveForm) {
        this.saveForm = saveForm;
    }

    public String getSaveFormName() {
        return saveFormName;
    }

    public void setSaveFormName(String saveFormName) {
        this.saveFormName = saveFormName;
    }

    
}

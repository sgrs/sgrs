package model;

import java.io.Serializable;

public class NsModel implements Serializable{
    private String prefix;
    private String uri;

    public NsModel(String prefix, String uri) {
        this.prefix = prefix;
        this.uri = uri;
    }

    public NsModel() {
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
    
    
}

package model.tripla;

import java.io.Serializable;

public class PredicateModel implements Serializable{
    private String predicate;

    public PredicateModel() {
        this.predicate="";
    }
    
    public PredicateModel(String predicate) {
        this.predicate = predicate;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        if(predicate!=null){
            this.predicate = predicate;
        }else{
            this.predicate = "";
        }
    }
    
    
    
    
}

package model.tripla;

import java.io.Serializable;

public class TriplaModel implements Serializable {  
    private SubjectModel subject;   
    private PredicateModel predicate;  
    private ObjectModel object;  
  
    public TriplaModel() {
        this.subject= new SubjectModel("");  
        this.predicate = new PredicateModel("");  
        this.object = new ObjectModel("");
    } 

    public TriplaModel(SubjectModel subject) {
        this.subject = subject;
        this.predicate = new PredicateModel("");  
        this.object = new ObjectModel("");
    }

    public TriplaModel(SubjectModel subject, PredicateModel predicate) {
        this.subject = subject;
        this.predicate = predicate; 
        this.object = new ObjectModel("");
    }
    
    public TriplaModel(SubjectModel subject, PredicateModel predicate, ObjectModel object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }
  
    public TriplaModel(String sujeito, String predicado, String objeto) {  
        this.subject= new SubjectModel(sujeito);  
        this.predicate = new PredicateModel(predicado);  
        this.object = new ObjectModel(objeto);   
    }  

    public SubjectModel getSubject() {
        return subject;
    }

    public void setSubject(SubjectModel subject) {
        this.subject = subject;
    }

    public PredicateModel getPredicate() {
        return predicate;
    }

    public void setPredicate(PredicateModel predicate) {
        this.predicate = predicate;
    }

    public ObjectModel getObject() {
        return object;
    }

    public void setObject(ObjectModel object) {
        this.object = object;
    }
  
} 

package model.tripla;

import java.io.Serializable;

public class ObjectModel implements Serializable{
    private String object;

    public ObjectModel() {
        this.object="";
    }

    public ObjectModel(String object) {
        this.object = object;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        if(object!=null){
            this.object = object;
        }else{
            this.object = "";
        }
    }
    
    
}

package model.tripla;

import java.io.Serializable;

public class SubjectModel implements Serializable {

    private String domain = "";
    private String subject = "";
    private String id = "";

    public SubjectModel(String domain, String sujeito, String id) {
        this.domain = domain;
        this.subject = sujeito;
        this.id = id;
    }

    public SubjectModel(String uri) {
        if (uri != null) {
            if (!uri.equals("")) {
                this.domain = uri;
            } else {
                this.domain = "http://www.exemplo.com/";
            }
        }
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        if (domain != null) {
            this.domain = domain;
        } else {
            this.domain = "";
        }
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject != null) {
            this.subject = subject;
        } else {
            this.subject = "";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id != null) {
            this.id = id;
        } else {
            this.id = "";
        }
    }

    public String getUri() {
        if (id.equals("") && subject.equals("")) {
            return domain;
        } else if (id.equals("")) {
            if (subject.endsWith("/")) {
                return domain + subject.substring(0, subject.length() - 1);
            } else {
                return domain + subject;
            }
        } else {
            if (subject.endsWith("/")) {
                return domain + subject + id;
            } else {
                return domain + subject + "/" + id;
            }
        }
    }
}

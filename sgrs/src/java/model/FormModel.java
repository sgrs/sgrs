package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import model.tripla.TriplaModel;

public class FormModel implements Serializable{
    private String uri;
    private String name;
    private String typeValue;
    private ArrayList<String> predicates;

    public FormModel() {
        this.uri="";
        this.name="";
        this.typeValue="";
        this.predicates=new ArrayList<>();
    }
    
    public FormModel(String name, List<TriplaModel> list) {
        this.uri="";
        this.name=name;
        this.typeValue="";
        this.predicates=new ArrayList<>();
        for(TriplaModel tripla : list){
            this.predicates.add(tripla.getPredicate().getPredicate());
            if(tripla.getPredicate().getPredicate().equals("rdf:type")){
                this.typeValue=tripla.getObject().getObject();
            }
        }
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getPredicates() {
        return predicates;
    }

    public void setPredicates(ArrayList<String> predicates) {
        this.predicates = predicates;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }
    
    
}

package model;

import model.tripla.TriplaModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GraphModel implements Serializable {
    
    private List<TriplaModel> triplas; 
    
    public GraphModel() {
        triplas = new ArrayList();
    }

    public GraphModel(List<TriplaModel> triplas) {
        this.triplas = triplas;
    }

    public List<TriplaModel> getTriplas() {
        return triplas;
    }

    public void setTriplas(List<TriplaModel> triplas) {
        this.triplas = triplas;
    }
    
    public void addTripla(TriplaModel tripla){
        this.triplas.add(tripla);
    }
    
    public void addTripla(int index, TriplaModel tripla){
        this.triplas.add(index, tripla);
    }
    
    public void removeTripla(TriplaModel tripla){
        this.triplas.remove(tripla);
    }
    
}

package dao;

public interface WebServiceConfiguration {
    public static String URL_WEB_SERVICE = "http://10.60.245.206:10035/repositories/";
    public static String URL_WEB_SESSION = "http://10.60.245.206:10035/session";
    //public static String URL_WEB_SERVICE = "http://192.168.0.106:10035/repositories/";
    //public static String URL_WEB_SESSION = "http://192.168.0.106:10035/session";
    
    public static String URL_REP_DATA = "dados";
    public static String URL_REP_ONTOLOGIES = "ontologias"; 
    public static String URL_REP_USERS = "usuarios";
    public static String URL_REP_FORMS = "forms";
}

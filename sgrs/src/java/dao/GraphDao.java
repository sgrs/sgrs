package dao;

import bean.GraphBean;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import model.GraphModel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import static dao.WebServiceConfiguration.URL_REP_ONTOLOGIES;
import static dao.WebServiceConfiguration.URL_REP_DATA;
import static dao.WebServiceConfiguration.URL_WEB_SERVICE;
import javax.json.JsonValue;
import model.NsModel;
import model.tripla.ObjectModel;
import model.tripla.PredicateModel;
import model.tripla.SubjectModel;
import model.tripla.TriplaModel;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.util.EntityUtils;

public class GraphDao implements WebServiceConfiguration{
    private NsDao nsDao = new NsDao();
    private static String sessionUri;
    
    public boolean insert(GraphModel g){
        Model model = getModelByGraph(g);
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_DATA+"/statements");
        request.setHeader("Content-Type", "text/turtle");
        try {
            StringWriter out = new StringWriter();
            model.write(out, "TURTLE");
            StringEntity triplas = new StringEntity(out.toString());
            request.setEntity(triplas);
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public int getLastId(String uri){
        int lastId=0;
        String sparql ="SELECT ?s WHERE {?s ?p ?o FILTER regex(str(?s), \"^"+uri+"\")} ORDER BY DESC (?s) LIMIT 1";
        HttpClient client = new DefaultHttpClient(); 
        
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_DATA+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            if(!json.isEmpty()){
                String id = json.toString().substring(json.toString().indexOf('<')+uri.length()+1, json.toString().indexOf('>'));
                if(Pattern.matches("^[0-9]", id)){
                    lastId=Integer.parseInt(id);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastId;
    }
    
    private Model getModelByGraph(GraphModel g){
        Model model = ModelFactory.createDefaultModel();
        int lastId = getLastId(g.getTriplas().get(0).getSubject().getUri()+"/");
        g.getTriplas().get(0).getSubject().setId(String.valueOf(lastId+1));
        Resource r = model.createResource(g.getTriplas().get(0).getSubject().getUri());
        for(TriplaModel tripla : g.getTriplas()){
            String predicate = convertPrefixToUri(tripla.getPredicate().getPredicate());
            String object = convertPrefixToUri(tripla.getObject().getObject());
            if(getPredicateType(tripla.getPredicate().getPredicate()).equals("http://www.w3.org/2002/07/owl#DatatypeProperty")){
                r.addProperty(model.createProperty(predicate), model.createLiteral(object));
            }else{
                r.addProperty(model.createProperty(predicate), model.createResource(object));
            }
        }
        return model;
    }
    
    private Model getModelByGraph(GraphModel g, String uri){
        Model model = ModelFactory.createDefaultModel();
        Resource r = model.createResource(uri);
        for(TriplaModel tripla : g.getTriplas()){
            String predicate = convertPrefixToUri(tripla.getPredicate().getPredicate());
            String object = convertPrefixToUri(tripla.getObject().getObject());
            if(getPredicateType(tripla.getPredicate().getPredicate()).equals("http://www.w3.org/2002/07/owl#DatatypeProperty")){
                r.addProperty(model.createProperty(predicate), model.createLiteral(object));
            }else{
                r.addProperty(model.createProperty(predicate), model.createResource(object));
            }
        }
        return model;
    }
    
    public List<String> getPredicatesWithInitial(String query){
        List<String> list = new ArrayList();
        String sparql ="SELECT DISTINCT ?s WHERE {?s rdf:type rdf:Property FILTER regex(str(?s), \"^"+convertPrefixToUri(query)+"\")} ORDER BY (?s) LIMIT 5";
        HttpClient client = new DefaultHttpClient();        
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            Iterator it = json.iterator();
            while(it.hasNext()){
                String text = it.next().toString();
                text = text.substring(text.indexOf('<')+1, text.indexOf('>'));
                list.add(convertUriToPrefix(text));
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<String> getPredicatesToClass(String classe){
        List<String> list = new ArrayList();
        String sparql ="SELECT DISTINCT ?s WHERE {?s rdf:type rdf:Property. ?s rdfs:domain <"+convertPrefixToUri(classe)+">} ORDER BY (?s)";
        HttpClient client = new DefaultHttpClient();        
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            Iterator it = json.iterator();
            while(it.hasNext()){
                String text = it.next().toString();
                text = text.substring(text.indexOf('<')+1, text.indexOf('>'));
                list.add(convertUriToPrefix(text));
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<String> getObjectsWithInitial(String predicate, String query){
        List<String> list = new ArrayList();
        String sparql ="SELECT DISTINCT ?s WHERE {<"+convertPrefixToUri(predicate)+"> rdfs:range ?z. ?s rdf:type ?z FILTER regex(str(?s), \"^"+convertPrefixToUri(query)+"\")} ORDER BY (?s) LIMIT 5";
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            Iterator it = json.iterator();
            while(it.hasNext()){
                String text = it.next().toString();
                text = text.substring(text.indexOf('<')+1, text.indexOf('>'));
                list.add(convertUriToPrefix(text));
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public String getPredicateType(String predicate){
        String result="";
        String sparql ="SELECT DISTINCT ?x WHERE {<"+convertPrefixToUri(predicate)+"> rdf:type ?x.}";
        HttpClient client = new DefaultHttpClient();        
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            Iterator it = json.iterator();
            while(it.hasNext()){
                String text = it.next().toString();
                text = text.substring(text.indexOf('<')+1, text.indexOf('>'));
                if(text.equals("http://www.w3.org/2002/07/owl#DatatypeProperty")){
                    result=text;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return convertUriToPrefix(result);
    }
    
    public String getRangeType(String query) {
        String result = "";
        String sparql = "SELECT DISTINCT ?o WHERE {<" + convertPrefixToUri(query) + "> rdfs:range ?o}";
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(sessionUri + "?query=" + URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            Iterator it = json.iterator();
            while (it.hasNext()) {
                String text = it.next().toString();
                text = text.substring(text.indexOf('<') + 1, text.indexOf('>'));
                result = text;
            }
        }catch (IOException ex){
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return convertUriToPrefix(result);
    }
    
    public String getSessionUri(){
        if(this.sessionUri==null){
            HttpClient client = new DefaultHttpClient();

            try {
                HttpPost request = new HttpPost(URL_WEB_SESSION+"?store=" + URLEncoder.encode("<"+URL_REP_ONTOLOGIES+">[rdfs++]+<"+URL_REP_DATA+">[rdfs++]", "UTF-8"));   
                HttpResponse response = client.execute(request);
                if(response.getStatusLine().getStatusCode()==200){
                    this.sessionUri = EntityUtils.toString(response.getEntity()); 
                }
            } catch (IOException ex) {
                Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.sessionUri;
    }
        
    public String convertPrefixToUri(String query) {
        for (NsModel ns : nsDao.getAllNS()) {
            if(query.contains(":")){
                if (ns.getPrefix().startsWith(query.substring(0, query.indexOf(":")))) {
                    query = query.replace(ns.getPrefix() + ":", ns.getUri());
                    break;
                }
            }
        }
        return query;
    }

    public String convertUriToPrefix(String uri) {
        for (NsModel ns : nsDao.getAllNS()) {
            if (uri.startsWith(ns.getUri())) {
                uri = uri.replace(ns.getUri(), ns.getPrefix() + ":");
                break;
            }
        }        
        return uri;
    }

    public List<GraphModel> search(GraphModel pattern) {
        List<GraphModel> graphs = new ArrayList<>(); 
        
        String sparql ="select ?s ?p ?o WHERE {";
        sparql+="?s ?p ?o FILTER regex(str(?s), \"^"+pattern.getTriplas().get(0).getSubject().getUri()+"\").";
        for(TriplaModel tripla : pattern.getTriplas()){
            if(!(tripla.getPredicate().getPredicate().equals("")) && !(tripla.getObject().getObject().equals(""))){
                sparql+="?s <"+convertPrefixToUri(tripla.getPredicate().getPredicate())+"> ?o1 FILTER regex(str(?o1), \"^"+convertPrefixToUri(tripla.getObject().getObject())+"\").";                
            }
        }
        sparql+="} ORDER BY (?s)";
        
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
        
            HttpResponse response = client.execute(request);
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            
            if(!json.isEmpty()){
                ArrayList<TriplaModel> triplas = new ArrayList<>();
                SubjectModel s = new SubjectModel("");
                PredicateModel p = new PredicateModel();
                ObjectModel o = new ObjectModel();
                for(JsonValue j : json){
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    if(!s.getUri().equals(values[0])){
                        graphs.add(new GraphModel(triplas));
                        triplas = new ArrayList<>();
//                        String sub = values[0].replace(s.getDomain(), "");
//                        sub = sub.substring(0, sub.lastIndexOf("/"));
//                        String id = values[0].substring(values[0].lastIndexOf("/")+1, values[0].length());
                        s = new SubjectModel(values[0]);
                        p = new PredicateModel();
                    }
                    if(!p.getPredicate().equals(convertUriToPrefix(values[1]))){
                        p = new PredicateModel(convertUriToPrefix(values[1]));
                    }
                    o = new ObjectModel(convertUriToPrefix(values[2]));
                    triplas.add(new TriplaModel(s, p, o));
                }     
                graphs.add(new GraphModel(triplas));
                graphs.remove(0);
            }                      
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return graphs;
    }
    
    
    public GraphModel searchByUri(String uri) {        
        String sparql ="select ?s ?p ?o WHERE {";
        sparql+="?s ?p ?o FILTER regex(str(?s), \"^"+uri+"\").";
        sparql+="} ORDER BY (?s)";
        
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(getSessionUri()+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
        
            HttpResponse response = client.execute(request);
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            
            if(!json.isEmpty()){
                GraphModel graph = new GraphModel();
                ArrayList<TriplaModel> triplas = new ArrayList<>();
                SubjectModel s = new SubjectModel("");
                PredicateModel p = new PredicateModel();
                ObjectModel o = new ObjectModel();
                for(JsonValue j : json){
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    if(!s.getUri().equals(values[0])){
                        triplas = new ArrayList<>();
//                        String sub = values[0].replace(s.getDomain(), "");
//                        sub = sub.substring(0, sub.lastIndexOf("/"));
//                        String id = values[0].substring(values[0].lastIndexOf("/")+1, values[0].length());
                        s = new SubjectModel(values[0]);
                        p = new PredicateModel();
                    }
                    if(!p.getPredicate().equals(convertUriToPrefix(values[1]))){
                        p = new PredicateModel(convertUriToPrefix(values[1]));
                    }
                    o = new ObjectModel(convertUriToPrefix(values[2]));
                    triplas.add(new TriplaModel(s, p, o));
                }     
                graph.setTriplas(triplas);
                return graph;
            }                      
        } catch (IOException ex) {
            Logger.getLogger(FormDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean delete(String subject, String predicate, String object){
        
        
        try {
            String sparql=URL_WEB_SERVICE+URL_REP_DATA+"/statements?";
            if(subject!=null){
                sparql += "subj="+URLEncoder.encode("<"+subject+">", "UTF-8")+"&";
            }
            if(predicate!=null){
                sparql += "pred="+URLEncoder.encode("<"+predicate+">", "UTF-8")+"&";
            }
            if(object!=null){
                sparql += "obj="+URLEncoder.encode("<"+object+">", "UTF-8")+"&";
            }
            
            HttpClient client = new DefaultHttpClient();
            HttpDelete request = new HttpDelete(sparql);
            System.out.println(request.getURI());
            request.setHeader("Content-Type", "text/turtle");
            HttpResponse response = client.execute(request);
            
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return false;
    }
    
    public boolean update(GraphModel g){
        if(delete(g.getTriplas().get(0).getSubject().getUri(), null, null)){         
            Model model = getModelByGraph(g, g.getTriplas().get(0).getSubject().getUri());
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_DATA+"/statements");
            request.setHeader("Content-Type", "text/turtle");
            try {
                StringWriter out = new StringWriter();
                model.write(out, "TURTLE");
                StringEntity triplas = new StringEntity(out.toString());
                request.setEntity(triplas);
                HttpResponse response = client.execute(request);
                return (response.getStatusLine().getStatusCode()==200);
            } catch (IOException ex) {
                Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }else{
            return false;
        }        
    }
}

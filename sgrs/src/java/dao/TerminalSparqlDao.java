package dao;

import static dao.WebServiceConfiguration.URL_REP_DATA;
import static dao.WebServiceConfiguration.URL_REP_ONTOLOGIES;
import static dao.WebServiceConfiguration.URL_WEB_SESSION;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.NsModel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

public class TerminalSparqlDao implements WebServiceConfiguration {

    private final String sessionUri = getSessionUrl();

    public Model search(String sparql, int limit, boolean reasoner, String repository) {
        Model model = ModelFactory.createDefaultModel();
        String uriConsulta;
        try {
            switch (repository) {
                case "dados":
                    uriConsulta = URL_WEB_SERVICE + URL_REP_DATA + "/statements";
                    break;
                case "ontologias":
                    uriConsulta = URL_WEB_SERVICE + URL_REP_ONTOLOGIES + "/statements";
                    break;
                default:
                    uriConsulta = sessionUri + "/statements";
                    break;
            }
            uriConsulta += "?query=" + URLEncoder.encode(sparql, "UTF-8");
            if (reasoner) {
                uriConsulta += "&infer=true";
            }
            if (limit > 0) {
                uriConsulta += "&limit=" + limit;
            }
            HttpClient client = new DefaultHttpClient();

            HttpGet request = new HttpGet(uriConsulta);
            System.out.print("+++" + request.getURI());
            request.addHeader("Accept", "application/rdf+xml");
            HttpResponse response = client.execute(request);
            //String aux = EntityUtils.toString(response.getEntity());
            model.read(response.getEntity().getContent(), "http://www.exemplo.com/", "RDF/XML");
            List<NsModel> ls = new NsDao().getAllNS();
            for(NsModel ns : ls){
                model.setNsPrefix(ns.getPrefix(), ns.getUri());
            }
        } catch (IOException ex) {
            Logger.getLogger(TerminalSparqlDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public String getSessionUrl() {
        HttpClient client = new DefaultHttpClient();

        try {
            HttpPost request = new HttpPost(URL_WEB_SESSION + "?store=" + URLEncoder.encode("<" + URL_REP_ONTOLOGIES + ">[rdfs++]+<" + URL_REP_DATA + ">[rdfs++]", "UTF-8"));
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(response.getEntity());
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}

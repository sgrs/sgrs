package dao;

import bean.GraphBean;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import static dao.WebServiceConfiguration.URL_REP_DATA;
import static dao.WebServiceConfiguration.URL_WEB_SERVICE;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonValue;
import model.FormModel;
import org.apache.http.client.methods.HttpDelete;

public class FormDao implements WebServiceConfiguration{
    
    public boolean insert(FormModel form){
        Model model = getModelByForm(form);
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_FORMS+"/statements");
        request.setHeader("Content-Type", "text/turtle");
        try {
            StringWriter out = new StringWriter();
            model.write(out, "TURTLE");
            StringEntity triplas = new StringEntity(out.toString());
            request.setEntity(triplas);
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    private Model getModelByForm(FormModel form){
        Model model = ModelFactory.createDefaultModel();
        int lastId = getLastId("http://www.exemplo.com/forms/");
        Resource r = model.createResource("http://www.exemplo.com/forms/"+(lastId+1));
        r.addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/formName"), form.getName());
        r.addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/formTypeValue"), model.createResource(form.getTypeValue()));
        for(String predicate : form.getPredicates()) {            
            r.addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/formPredicate"), model.createResource(predicate));            
        }
        return model;
    }
    
    public int getLastId(String uri){
        int lastId=0;
        String sparql ="SELECT ?s WHERE {?s ?p ?o FILTER regex(str(?s), \"^"+uri+"\")} ORDER BY DESC (?s) LIMIT 1";
        HttpClient client = new DefaultHttpClient(); 
        
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_DATA+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            if(!json.isEmpty()){
                String id = json.toString().substring(json.toString().indexOf('<')+uri.length()+1, json.toString().indexOf('>'));
                if(Pattern.matches("^[0-9]", id)){
                    lastId=Integer.parseInt(id);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastId;
    }
    
    public List<FormModel> getAll(){
        List<FormModel> forms = new ArrayList<>();  

        String sparql ="select ?s ?p ?o WHERE {?s ?p ?o. } ORDER BY ?s";
        
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_FORMS+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
        
            HttpResponse response = client.execute(request);
            //JsonArray json = Json.createReader(response.getEntity().getContent()).readArray();
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            
            if(!json.isEmpty()){
                FormModel form = new FormModel();
                for(JsonValue j : json){
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    if(!form.getUri().equals(values[0])){
                        forms.add(form);//A primeira adição é de um form vazio. No final será removido
                        form = new FormModel();
                        form.setUri(values[0]);
                    }
                    switch (values[1]) {
                        case "http://www.exemplo.com/grs/ontology/formName":
                            form.setName(values[2]);
                            break;
                        case "http://www.exemplo.com/grs/ontology/formTypeValue":
                            form.setTypeValue(values[2]);
                            break;
                        case "http://www.exemplo.com/grs/ontology/formPredicate":
                            form.getPredicates().add(values[2]);
                            break;
                        default:
                            break;
                    }
                }
                forms.add(form);//Adiciona o último form
                forms.remove(0);//Remove o primeiro form adicionado, pois é vazio.
                return forms;
            }                      
        } catch (IOException ex) {
            Logger.getLogger(FormDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return forms;
    }
    
    public boolean delete(String subject, String predicate, String object){
        String sparql="";
        if(subject!=null){
            sparql += "subj=<"+subject+">&";
        }
        if(predicate!=null){
            sparql += "subj=<"+subject+">&";
        }
        if(object!=null){
            sparql += "subj=<"+subject+">&";
        }
        HttpClient client = new DefaultHttpClient();
        
        try {
            HttpDelete request = new HttpDelete(URL_WEB_SERVICE+URL_REP_FORMS+"/statements?"+URLEncoder.encode(sparql, "UTF-8"));
            request.setHeader("Content-Type", "text/turtle");
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }
}

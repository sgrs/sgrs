package dao;

import bean.GraphBean;
import static dao.WebServiceConfiguration.URL_WEB_SERVICE;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonValue;
import model.UserModel;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import static dao.WebServiceConfiguration.URL_REP_USERS;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    
    public UserModel logar(String email, String senha){
        UserModel user;
        //Por enquanto essa consulta não está verificando se o usuário está ativado ou não. 
        //Posteriormente mudaremos isso para que só usuário ativado possa logar.       
        String sparql ="select ?s ?p ?o WHERE{?s ?p ?o. ?s foaf:mbox \""+email+"\". ?s <http://www.exemplo.com/grs/ontology/passwd> \""+hash(senha)+"\".}";
        HttpClient client = new DefaultHttpClient(); 
        
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_USERS+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            if(!json.isEmpty()){
                user = new UserModel();
                for(JsonValue j : json){                
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    user.setUri(values[0]);
                    switch (values[1]) {
                        case "http://xmlns.com/foaf/0.1/name":
                            user.setName(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/mbox":
                            user.setEmail(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/phone":
                            user.setPhone(values[2]);
                            break;
                        case "http://www.exemplo.com/grs/ontology/passwd":
                            user.setPasswd(values[2]);
                            break;
                        case "http://www.w3.org/1999/02/22-rdf-syntax-ns#type":
                            switch (values[2]) {
                                case "http://www.exemplo.com/grs/ontology/Disabled":
                                    user.setActivated(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Activated":
                                    user.setActivated(true);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Common":
                                    user.setAdm(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Administrator":
                                    user.setAdm(true);
                                    break;
                                default:
                                    break;
                            }   break;
                        default:
                            break;
                    }
                }
                return user;
            }
            
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean insert(UserModel user){
        Model model = getModelByUser(user);
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_USERS+"/statements");
        request.setHeader("Content-Type", "text/turtle");
        try {
            StringWriter out = new StringWriter();
            model.write(out, "TURTLE");
            StringEntity triplas = new StringEntity(out.toString());
            request.setEntity(triplas);
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    private Model getModelByUser(UserModel user, String uri){
        Model model = ModelFactory.createDefaultModel();
        Resource r = model.createResource(uri);
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/name"), user.getName());
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/mbox"), user.getEmail());
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/phone"), user.getPhone());
        if(user.isAdm()){
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Administrator"));
        }else{
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Common"));
        }
        if(user.isActivated()){
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Activated"));
        }else{
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Disabled"));
        }
        r.addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/passwd"), hash(user.getPasswd()));
        
        return model;
    }
    
    private Model getModelByUser(UserModel user){
        Model model = ModelFactory.createDefaultModel();
        int lastId = getLastId();
        Resource r = model.createResource("http://www.exemplo.com/grs/users/"+(lastId+1));
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/name"), user.getName());
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/mbox"), user.getEmail());
        r.addProperty(model.createProperty("http://xmlns.com/foaf/0.1/phone"), user.getPhone());
        if(user.isAdm()){
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Administrator"));
        }else{
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Common"));
        }
        if(user.isActivated()){
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Activated"));
        }else{
            r.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://www.exemplo.com/grs/ontology/Disabled"));
        }
        r.addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/passwd"), hash(user.getPasswd()));
        
        return model;
    }
    
    public int getLastId(){
        int lastId=0;
        String uri = "http://www.exemplo.com/grs/users/";
        String sparql ="SELECT ?s WHERE {?s ?p ?o FILTER regex(str(?s), \"^"+uri+"\")} ORDER BY DESC (?s) LIMIT 1";
        HttpClient client = new DefaultHttpClient(); 
        
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_USERS+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            if(!json.isEmpty()){
                String id = json.toString().substring(json.toString().indexOf('<')+uri.length()+1, json.toString().indexOf('>'));
                if(Pattern.matches("^[0-9]", id)){
                    lastId=Integer.parseInt(id);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastId;
    }
    
    public String hash(String passwd){
        
        return DigestUtils.sha256Hex(passwd);
    }
    
    public boolean delete(String subject, String predicate, String object){
       
        
        try {
            String sparql=URL_WEB_SERVICE+URL_REP_USERS+"/statements?";
            if(subject!=null){
                sparql += "subj="+URLEncoder.encode("<"+subject+">", "UTF-8")+"&";
            }
            if(predicate!=null){
                sparql += "pred="+URLEncoder.encode("<"+predicate+">", "UTF-8")+"&";
            }
            if(object!=null){
                sparql += "obj="+URLEncoder.encode("<"+object+">", "UTF-8")+"&";
            }
            
            HttpClient client = new DefaultHttpClient();
            HttpDelete request = new HttpDelete(sparql);
            request.setHeader("Content-Type", "text/turtle");
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean update(UserModel user){
        if(delete(user.getUri(), null, null)){         
            Model model = getModelByUser(user, user.getUri());
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_USERS+"/statements");
            request.setHeader("Content-Type", "text/turtle");
            try {
                StringWriter out = new StringWriter();
                model.write(out, "TURTLE");
                StringEntity triplas = new StringEntity(out.toString());
                request.setEntity(triplas);
                HttpResponse response = client.execute(request);
                return (response.getStatusLine().getStatusCode()==200);
            } catch (IOException ex) {
                Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }else{
            return false;
        }        
    }
    
    public boolean changePasswd(String email, String passwd){
        UserModel user = getUserByEmail(email);
        if(user!=null){
            delete(user.getUri(), "http://www.exemplo.com/grs/ontology/passwd", null);
            Model model =  ModelFactory.createDefaultModel();
            model.createResource(user.getUri())
                    .addProperty(model.createProperty("http://www.exemplo.com/grs/ontology/passwd"), hash(passwd));

            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_USERS+"/statements");
            request.setHeader("Content-Type", "text/turtle");
            try {
                StringWriter out = new StringWriter();
                model.write(out, "TURTLE");
                StringEntity triplas = new StringEntity(out.toString());
                request.setEntity(triplas);
                HttpResponse response = client.execute(request);
                return (response.getStatusLine().getStatusCode()==200);
            } catch (IOException ex) {
                Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }  
        }else{
            return false;
        }
    }
    
    public UserModel getUserByEmail(String email){
        UserModel user;
        String sparql ="select ?s ?p ?o WHERE{?s ?p ?o. ?s foaf:mbox \""+email+"\".}";
        HttpClient client = new DefaultHttpClient(); 
        
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_USERS+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);            
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            if(!json.isEmpty()){
                user = new UserModel();
                for(JsonValue j : json){                
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    user.setUri(values[0]);
                    switch (values[1]) {
                        case "http://xmlns.com/foaf/0.1/name":
                            user.setName(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/mbox":
                            user.setEmail(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/phone":
                            user.setPhone(values[2]);
                            break;
                        case "http://www.exemplo.com/grs/ontology/passwd":
                            user.setPasswd(values[2]);
                            break;
                        case "http://www.w3.org/1999/02/22-rdf-syntax-ns#type":
                            switch (values[2]) {
                                case "http://www.exemplo.com/grs/ontology/Disabled":
                                    user.setActivated(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Activated":
                                    user.setActivated(true);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Common":
                                    user.setAdm(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Administrator":
                                    user.setAdm(true);
                                    break;
                                default:
                                    break;
                            }   break;
                        default:
                            break;
                    }
                }
                return user;
            }
            
        } catch (IOException ex) {
            Logger.getLogger(GraphDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<UserModel> getAll() {
        List<UserModel> users = new ArrayList<>(); 
        
        String sparql ="select ?s ?p ?o WHERE { ?s ?p ?o } ORDER BY (?s)";
        
        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_USERS+"?query="+URLEncoder.encode(sparql, "UTF-8"));
            request.addHeader("Accept", "application/json");
        
            HttpResponse response = client.execute(request);
            JsonArray json = Json.createReader(response.getEntity().getContent()).readObject().getJsonArray("values");
            
            if(!json.isEmpty()){
                UserModel user = new UserModel();
                user.setUri("");
                for(JsonValue j : json){
                    System.out.println(j.toString());
                    String[] values = j.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("<", "").replaceAll(">", "").replaceAll("\\\\", "").trim().split(",");
                    if(!user.getUri().equals(values[0])){
                        users.add(user);
                        user = new UserModel();
                        user.setUri(values[0]);
                    }
                    switch (values[1]) {
                        case "http://xmlns.com/foaf/0.1/name":
                            user.setName(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/mbox":
                            user.setEmail(values[2]);
                            break;
                        case "http://xmlns.com/foaf/0.1/phone":
                            user.setPhone(values[2]);
                            break;
                        case "http://www.exemplo.com/grs/ontology/passwd":
                            user.setPasswd(values[2]);
                            break;
                        case "http://www.w3.org/1999/02/22-rdf-syntax-ns#type":
                            switch (values[2]) {
                                case "http://www.exemplo.com/grs/ontology/Disabled":
                                    user.setActivated(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Activated":
                                    user.setActivated(true);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Common":
                                    user.setAdm(false);
                                    break;
                                case "http://www.exemplo.com/grs/ontology/Administrator":
                                    user.setAdm(true);
                                    break;
                                default:
                                    break;
                            }   break;
                        default:
                            break;
                    }
                }     
                users.add(user);
                users.remove(0);
            }                      
        } catch (IOException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }
}

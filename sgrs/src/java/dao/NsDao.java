package dao;

import bean.NsBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import model.NsModel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class NsDao implements WebServiceConfiguration{
    
    private static List<NsModel> allNS;

    public NsDao() {
        if(allNS==null){
            allNS = this.getAll();
        }
    }

    public List<NsModel> getAllNS() {
        return allNS;
    }
    
    public List<NsModel> getAll(){
        List<NsModel> nss = new ArrayList<>();  

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(URL_WEB_SERVICE+URL_REP_DATA+"/namespaces");
        request.addHeader("Accept", "application/json");
        try {
            HttpResponse response = client.execute(request);
            JsonArray json = Json.createReader(response.getEntity().getContent()).readArray();
            for(int i=0; i<json.size();i++){
                NsModel ns = new NsModel(json.getJsonObject(i).getString("prefix"),json.getJsonObject(i).getString("namespace"));  
                nss.add(ns);
            }           
        } catch (IOException ex) {
            Logger.getLogger(NsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return nss;
    }
    
    public boolean insert(NsModel ns){
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_DATA+"/namespaces/"+ns.getPrefix());
        request.setHeader("Content-Type", "text/plain");
        try {
            StringEntity uri = new StringEntity(ns.getUri());
            request.setEntity(uri);
            HttpResponse response = client.execute(request);
            allNS = this.getAll();
            return (response.getStatusLine().getStatusCode()==204);
        } catch (IOException ex) {
            Logger.getLogger(NsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean delete(NsModel ns) {
        HttpClient client = new DefaultHttpClient();
        HttpDelete request = new HttpDelete(URL_WEB_SERVICE+URL_REP_DATA+"/namespaces/"+ns.getPrefix());
        try {
            HttpResponse response = client.execute(request);
            allNS = this.getAll();
            return (response.getStatusLine().getStatusCode()==204);
        } catch (IOException ex) {
            Logger.getLogger(NsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}

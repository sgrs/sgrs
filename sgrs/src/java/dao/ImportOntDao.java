package dao;

import bean.GraphBean;
import static dao.WebServiceConfiguration.URL_WEB_SERVICE;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jena.rdf.model.Model;

public class ImportOntDao implements WebServiceConfiguration{
    
    public boolean insert(Model model){
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL_WEB_SERVICE+URL_REP_ONTOLOGIES+"/statements");
        request.setHeader("Content-Type", "text/turtle");
        try {
            StringWriter out = new StringWriter();
            model.write(out, "TURTLE");
            StringEntity triplas = new StringEntity(out.toString());
            request.setEntity(triplas);
            HttpResponse response = client.execute(request);
            return (response.getStatusLine().getStatusCode()==200);
        } catch (IOException ex) {
            Logger.getLogger(GraphBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
